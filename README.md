# Lab Work 2 | Teoria Algoritmica da Informação

## Instruções para executar a aplicação em Linux
### Requerimentos
- g++
- cmake (versão 3.14 ou superior)

### Antes de compilar
De forma a ser possível correr a aplicação corretamente é necessário executar o seguinte script:
```
python ./prepareEnv.py
```

Este script criará a seguinte estrutura de pastas:

```
| results
|_
  | codebooks
  | histograms
  | quantization
```
Estas pastas receberão os ficheiro gerados pelos vários programas, com a exceção do programa codebooks que pode receber como parâmetro uma outra pasta de destino.

### Como compilar

#### CMake
Dentro da pasta "tai-trab2/src-example", onde contém o ficheiro CmakeList, fazer o comando:

```
cmake .
```

#### Compilador

```
* g++ wavhist.cpp wavhist.h -o wavhist
* g++ wavquant.cpp wavquant.h -o wavquant
* g++ wavcmp.cpp wavcmp.h -o wavcmp
* g++ wavcb.cpp wavcb.h -o wavcb
* g++ wavfind.cpp wavfind.h -o wavfind
```

### Executar

* Exemplo para execução do programa (`./[nome_programa] [parâmetros]`):

  * WAVhist
    * `./wavhist [input_file] [channel]`
    ```
    Canal esquerdo:
    ./wavhist ../WAVfiles/sample01.wav 0

    Canal direito:
    ./wavhist ../WAVfiles/sample01.wav 1

    Para mono:
    ./wavhist ../WAVfiles/sample01.wav -1
    ```
  * WAVquant
    * `./wavquant [shift_steps] [input_file] [output_file_name]`
    ```
    ./wavquant 12 ../WAVfiles/sample01.wav new_sample01
    ```  
  * WAVcmp
    * `./wavcmp [input_file] [original_file]`
    ```
    ./wavcmp ../results/quantization/sample01.wav ../WAVfiles/sample01.wav
    ```
  * WAVcb
    * `./wavcb [input_file/folder] [block_size] [overlap_percentage] [codebook_size] <optional: [-f [codebook_output_folder]]>`
    ```
    Criar codebook para ficheiro WAV:
    ./wavcb ../WAVfiles/sample01.wav 500 10 40
    
    Criar codebooks para todos os ficheiros WAV num diretorio:
    ./wavcb ../WAVfiles/ 500 10 40
    ```
    * Parâmetros opcionais:
    ```
    ./wavcb ../WAVfiles/sample01.wav 500 10 40 -f ./nova_pasta-destino
    ```
  * WAVfind
    * `./wavfind [input_file] [codebook_folder]`
    ```
    ./wavfind ../WAVfiles/sample01.wav ../results/codebooks
    ```

#### Nota:
Além da pasta WAVfiles, também existe uma pasta samples com mais ficheiros de áudio para teste.


### Comandos úteis
- Dar permissão de execucao aos programas:

```
sudo chmod 744 [nome_programa/script]
```
## Programado com recurso a
* CLion 2019
* Cygwin

## Autores
* **André Moreira** - **Hélder Paraense**

## Sobre
* Trabalho efectuado para a unidade curricular Teoria Algoritmica da Informação do mestrado de Engenharia Informática da Universidade de Aveiro. Entregue a 2019/11/24
import os

path = {'codebooks', 'quantization', 'histograms'}
created = False

print("Preparing envorinment...")
for p in path:
    # folder = os.path.join("..", "results", p)
    folder = os.path.join("results", p)
    if not os.path.exists("results"):
        print("Creating 'results' folder...\n'results' folder created!", end="\n\n")
    if not os.path.exists(folder):
        os.makedirs(folder)
        print("Creating ", folder, " folder...\n", folder, " created!", sep="'", end="\n\n")
        created = True
if created:
    print("Environment is ready!")
else:
    print("Nothing to do here!")
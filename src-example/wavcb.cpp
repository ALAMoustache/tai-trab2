//
// Created by André Moreira on 14/11/2019.
//

#include <iostream>
#include <sndfile.hh>
#include <vector>
#include <numeric>
#include <cstring>
#include "wavcb.h"
#include <boost/algorithm/string.hpp>
//#include <boost/filesystem.hpp>

using namespace std;

constexpr size_t FRAMES_BUFFER_SIZE = 65536; // Buffer for reading frames

//#include <experimental/filesystem> // or #include <filesystem>

//namespace fs = std::experimental::filesystem;
//namespace filesys = boost::filesystem;
/*
Check if given string path is of a Directory
*/
//bool checkIfDirectory(std::string filePath)
//{
//    try {
//        // Create a Path object from given path string
//        filesys::path pathObj(filePath);
//        // Check if path exists and is of a directory file
//        if (filesys::exists(pathObj) && filesys::is_directory(pathObj))
//            return true;
//    }
//    catch (filesys::filesystem_error & e)
//    {
//        std::cerr << e.what() << std::endl;
//    }
//    return false;
//}

const char kPathSeparator =
    #if defined _WIN32 || defined __CYGWIN__
            '\\';
    #else
    '/';
    #endif

int main(int argc, char *argv[]) {

//    cout << "separator: " << kPathSeparator << endl;

    if (argc < 5) {
        cerr << "Usage: wavcb <input file or folder> <block size> <overlap factor (%)> <cobebook size> "
                "<optional -f [output_folder_name]>" << endl;
        return 1;
    }

    int optional_args = argc - 5;
//    cout << optional_args << " " << string(argv[argc-(optional_args+4)]) << endl;
    string output_folder = string("results") + kPathSeparator + string("codebooks");
    for (int i = 5; i < argc; ++i){
        if (strcmp(argv[i], "-f") != 0)
            output_folder = string(argv[i++]);
    }

    vector<SndfileHandle> sndFilesVector;
    vector<string> fileNames;
    SndfileHandle testInput{argv[argc - (optional_args + 4)] };
    if (testInput.error()) {
        string filePath = argv[argc - (optional_args + 4)];
        fileNames = WAVCb::listWAVFiles(argv[argc - (optional_args + 4)]);
        for ( const auto& file : fileNames ) {
            SndfileHandle newSndFile{ filePath + kPathSeparator + file };
            sndFilesVector.push_back(newSndFile);
        }
        if (sndFilesVector.empty()) {
            cerr << "Error: invalid input file or folder" << endl;
            return 1;
        }
    } else {
        sndFilesVector.push_back(testInput);
        fileNames.push_back(argv[argc - (optional_args + 4)]);
    }

    const int block_size { stoi(argv[argc - (optional_args + 3)]) };
    if(block_size <= 0) {
        cerr << "Error: invalid block size" << endl;
        return 1;
    }

    auto overlap_factor_arg { stoi(argv[argc - (optional_args + 2)]) };
    if(overlap_factor_arg < 0 || overlap_factor_arg > 100) {
        cerr << "Error: invalid overlap factor" << endl;
        return 1;
    }

    auto overlap_factor = block_size*overlap_factor_arg/100.0;
    if(overlap_factor == block_size) {
        cerr << "Error: invalid overlap factor" << endl;
        return 1;
    }

    const short codebook_size { (short) stoi(argv[argc - (optional_args + 1)]) };
    if(codebook_size <= 0 || codebook_size == block_size) {
        cerr << "Error: invalid codebook size" << endl;
        return 1;
    }

    for (int i = 0; i < sndFilesVector.size(); i++) {
        if (sndFilesVector[i].error()) {
            cerr << "Error: invalid input file" << endl;
            return 1;
        }

        if ((sndFilesVector[i].format() & SF_FORMAT_TYPEMASK) != SF_FORMAT_WAV) {
            cerr << "Error: file is not in WAV format" << endl;
            return 1;
        }

        if ((sndFilesVector[i].format() & SF_FORMAT_SUBMASK) != SF_FORMAT_PCM_16) {
            cerr << "Error: file is not in PCM_16 format" << endl;
            return 1;
        }

        string input_filename = fileNames[i];

        vector<string> strs;
        boost::split(strs, input_filename, boost::is_any_of("\\/"));
//    cout << "strs: " << strs.back() << endl;
        const auto codebook_filename = strs.back().append(".cb");
        const auto codebook_output_path = string("..").append(
                kPathSeparator + output_folder + kPathSeparator + codebook_filename);
        cout << "output: " << codebook_output_path << endl;

        WAVCb codebook{sndFilesVector[i], block_size, overlap_factor, codebook_size, codebook_output_path};

        size_t nFrames;
        vector<short> samples(FRAMES_BUFFER_SIZE * sndFilesVector[i].channels());
        while ((nFrames = sndFilesVector[i].readf(samples.data(), FRAMES_BUFFER_SIZE))) {
            codebook.update(samples);
        }

        codebook.generateCodebook();
    }

    return 0;
}
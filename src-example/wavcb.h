//
// Created by André Moreira on 14/11/2019.
//

#ifndef SHAZEEM_WAVCB_H
#define SHAZEEM_WAVCB_H


#include <fstream>
#include <random>
#include <algorithm>
#include <map>
#include <vector>
#include <iostream>
#include <dirent.h>
#include "kmeans.h"
#include "wavhist.h"

class WAVCb {
    private:
        std::ofstream outputFile;
        short codebook_size, block_size, max_iterations;
        double overlap_factor;
        std::vector<short> mono;
        std::vector<std::vector<short>> channels;

    public:
        WAVCb(const SndfileHandle& sfh, int block_size, double overlap_factor, short codebook_size, std::string codebook_name) {
            this->channels.resize(sfh.channels());
            this->outputFile.open(codebook_name, ios::out);
            this->block_size = (short) block_size;
            this->overlap_factor = overlap_factor;
            this->codebook_size = codebook_size;
            this->max_iterations = 200;
        }

        void update(const std::vector<short>& samples) {
            int n = 0;

            for(auto s : samples){
                this->channels[n++ % (this->channels.size())].push_back(s);
            }
        }

        void generateCodebook(){
            if (this->channels.size() == 1){
                this->mono = this->channels[0];
            }else{
                WAVHist wavhist { };
                this->mono = wavhist.getMonoSamples(this->channels);
            }

            std::vector<Point> points;
            size_t n {};
            std::cout << "Overlap: " << this->overlap_factor << "\n";
            for( size_t i = 0; i < this->mono.size(); i += (this->block_size - this->overlap_factor) ) {

                Point point(n++, this->block_size);

                for( int j = 0; j < this->block_size; j++) {
                    point.addValue(this->mono[i+j]);
                }

                points.push_back(point);
            }

            /* FIM CRIAR OS VECTORES (PONTOS) */
            std::cout << "Mono: " << this->mono.size() << '\n';
            std::cout << "Points: " << points.size() << '\n';
            KMeans kmeans(this->codebook_size, points.size(), this->block_size, this->max_iterations);
            kmeans.run(points);

            std::vector<double> codebook;
            codebook = kmeans.getCentroidesValues();

            this->writeCodebook(codebook);
        }

        void writeCodebook(const std::vector<double>& codebook) {
            size_t counter = 1;
            for (auto value : codebook) {
                this->outputFile << value << ' ';
                if (counter % this->block_size == 0)
                    this->outputFile << '\n';

                counter++;
            }

            this->close();
        }

        void close() {
            if (this->outputFile.is_open()) {
                this->outputFile.close();
            }
        }

    static std::vector<std::string> listWAVFiles(const char *folder) {
        std::string WAVExtension = ".wav";
        size_t fileLength;
        DIR *directory;
        struct dirent *entry;
        directory = opendir(folder);
        std::vector<std::string> files;
        if (directory) {
            entry = readdir(directory);
            while (entry) {
                if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                    std::string fname = entry->d_name;
                    fileLength = size(fname);
                    std::string fileExtension = fname.substr(fileLength-4, fileLength);
                    if(fileExtension == WAVExtension)
                        cout << entry->d_name << '\n';
                        files.push_back(entry->d_name);
                }
                entry = readdir(directory);
            }
            closedir(directory);
        }
        return files;
    }
};


#endif //SHAZEEM_WAVCB_H

//
// Created by Helder Paraense on 16/11/19.
//
#include <iostream>
#include <vector>
#include <sndfile.hh>
#include "wavcmp.h"

using namespace std;

constexpr size_t FRAMES_BUFFER_SIZE = 65536; // Buffer for reading frames

int main(int argc, char *argv[]) {
    if (argc < 3) {
        cerr << "Usage: wavcmp <input file> <original file>" << endl;
        return 1;
    }

    cout << "Input File: " << argv[argc - 2] << endl;
    SndfileHandle inputSndFile{argv[argc - 2]};
    if (inputSndFile.error()) {
        cerr << "Error: invalid input file" << endl;
        return 1;
    }

    if ((inputSndFile.format() & SF_FORMAT_TYPEMASK) != SF_FORMAT_WAV) {
        cerr << "Error: file is not in WAV format" << endl;
        return 1;
    }

    if ((inputSndFile.format() & SF_FORMAT_SUBMASK) != SF_FORMAT_PCM_16) {
        cerr << "Error: file is not in PCM_16 format" << endl;
        return 1;
    }

    SndfileHandle originalSndFile{argv[argc - 1]};
    if (originalSndFile.error()) {
        cerr << "Error: invalid original file" << endl;
        return 1;
    }

    if ((originalSndFile.format() & SF_FORMAT_TYPEMASK) != SF_FORMAT_WAV) {
        cerr << "Error: original file is not in WAV format" << endl;
        return 1;
    }

    if ((originalSndFile.format() & SF_FORMAT_SUBMASK) != SF_FORMAT_PCM_16) {
        cerr << "Error: original file is not in PCM_16 format" << endl;
        return 1;
    }

    if (!WAVCmp::compareHeaders(originalSndFile, inputSndFile)) {
        cerr << "Error: files don't have same properties" << endl;
        return 1;
    }

    size_t nInputFrames;
    size_t nOriginalFrames;

    long signal = 0;
    long noise = 0;
    long maximumError = 0;
    vector<long> noiseMaxErrorVec;

    vector<short> inputSamples(FRAMES_BUFFER_SIZE * inputSndFile.channels());
    vector<short> originalSamples(FRAMES_BUFFER_SIZE * originalSndFile.channels());

    nInputFrames = inputSndFile.readf(inputSamples.data(), FRAMES_BUFFER_SIZE);
    nOriginalFrames = originalSndFile.readf(originalSamples.data(), FRAMES_BUFFER_SIZE);
    while (nInputFrames && nOriginalFrames) {
        inputSamples.resize(nInputFrames * inputSndFile.channels());
        originalSamples.resize(nOriginalFrames * originalSndFile.channels());

        signal += WAVCmp::calculateEnergy(originalSamples);
        noiseMaxErrorVec = WAVCmp::calculateNoise(originalSamples, inputSamples);

        maximumError = max(maximumError, noiseMaxErrorVec[0]);
        noise += noiseMaxErrorVec[1];
        nInputFrames = inputSndFile.readf(inputSamples.data(), FRAMES_BUFFER_SIZE);
        nOriginalFrames = originalSndFile.readf(originalSamples.data(), FRAMES_BUFFER_SIZE);
    }

    long double SNR = WAVCmp::SNR(signal, noise);

    cout << "Maximum per sample error: " << maximumError << endl;
    cout << "SNR: " << SNR << endl;

    return 0;
}
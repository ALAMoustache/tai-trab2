//
// Created by Helder Paraense on 16/11/19.
//

#ifndef SHAZEEM_WAVCMP_H
#define SHAZEEM_WAVCMP_H

#include <cmath>


class WAVCmp {
/*
private:
    std::vector<std::vector<short>> sample;
    double energy_signal, energy_noise;
*/

public:
/*    WAVCmp() {
        this->energy_signal = 0;
        this->energy_noise = 0;
        sample.resize(2);
    }*/

    static bool compareHeaders(const SndfileHandle &originalSndFile, const SndfileHandle &inputSndFile) {
        return originalSndFile.frames() == inputSndFile.frames() &&
               originalSndFile.samplerate() == inputSndFile.samplerate() &&
               originalSndFile.channels() == inputSndFile.channels() &&
               originalSndFile.format() == inputSndFile.format();
    }

    static long calculateEnergy(std::vector<short> &originalSamples) {
        long signal = 0;
        for (short originalSample : originalSamples) {
            signal += pow(originalSample, 2);
        }

        return signal;
    }

    static std::vector<long> calculateNoise(std::vector<short> &originalSamples, std::vector<short> &inputSamples) {
        std::vector<long> output = {0, 0};
        long auxMax;
        for (size_t i = 0; i < originalSamples.size(); i++) {
            auxMax = originalSamples[i] - inputSamples[i];
            auxMax = std::max(auxMax, output[0]);
            output[0] = auxMax;
            output[1] += pow(originalSamples[i] - inputSamples[i], 2);
        }
        return output;
    }

    static long double SNR(long double signal, long double noise) {
        return 10 * log10l(signal / noise);
    }


/*    void setSample (std::vector<std::vector<short>> sample) {
        this->sample = sample;
    }

    std::vector<double> calcSNR() {
        std::vector<double> result;
        float snr;
        int max_absolute_error = 0, per_sample_abs_err = 0, sample_diff = 0;
        size_t min_size = sample[0].size();
        if (min_size > sample[1].size()){
            min_size = sample[1].size();
        }

        for(size_t i=0; i<min_size; i++){
            sample_diff = sample[0][i]-sample[1][i];
            energy_signal += pow(sample[0][i],2);
            energy_noise += pow(sample_diff,2);
            per_sample_abs_err = abs(sample_diff);
            if(max_absolute_error < per_sample_abs_err){
                max_absolute_error = per_sample_abs_err;
            }
        }


        if (energy_noise == 0){
            snr = 0;
        } else {
            snr = 10 * log10 ((float)energy_signal / (float)energy_noise);
        }

        std::cout << "Energy: " << energy_signal << "\n";
        std::cout << "Noise: " << energy_noise << "\n";

        result.push_back(snr);
        result.push_back(max_absolute_error);

        return result;
    }*/
};


#endif //SHAZEEM_WAVCMP_H

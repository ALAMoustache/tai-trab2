//
// Created by Helder Paraense on 20/11/19.
// André Moreira 23/11/2019
//

#ifndef SHAZEEM_WAVFIND_H
#define SHAZEEM_WAVFIND_H

#include <dirent.h>
#include <sstream>
#include "wavhist.h"
#include "wavcmp.h"

class WAVFind {
private:
    std::vector<std::vector<short>> channels;
    const char* codebook_folder;
    double error;
    std::vector<std::string> files;
    std::vector<short> mono;
    std::string filename;

public:
    WAVFind(const SndfileHandle &sfh, const char* codebook_folder) {
        this->channels.resize(sfh.channels());
        this->codebook_folder = codebook_folder;
        this->error = -1;
    }

    void update(const std::vector<short> &samples) {
        int n = 0;

        for (auto s : samples) {
            this->channels[n++ % (this->channels.size())].push_back(s);
        }
    }

    void listCodebooks() {
        DIR *directory;
        struct dirent *entry;
        directory = opendir(this->codebook_folder);
        if (directory) {
            entry = readdir(directory);
            while (entry) {
                if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
                    this->files.push_back(entry->d_name);
                entry = readdir(directory);
            }
            closedir(directory);
        }
    }

    void findMusic(){
        clock_t begin = clock();

        listCodebooks();

        if (this->channels.size() == 1){
            this->mono = this->channels[0];
        }else{
            WAVHist hist { };
            this->mono = hist.getMonoSamples(this->channels);
        }

        for(size_t i=0; i < this->files.size(); i++){
            std::cout << '\n' << this->files[i] << '\n';
            std::vector<std::vector<double>> codebook = readCodebook(this->files[i]);
            double snr = getError(codebook);

            if(snr > this->error || this->error == -1){
                this->error = snr;
                this->filename = this->files[i];
            }

            std::cout << "SNR: " << snr << '\n';
        }

        std::cout << "\nMatches closer to: " << this->filename << '\n';

        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "Elapsed time: " <<  elapsed_secs << '\n';
    }

    std::vector<std::vector<double>> readCodebook(const std::string cbk){
        std::vector<std::vector<double>> codebook;
        std::ifstream input_file(std::string(this->codebook_folder) + kPathSeparator + cbk);
        std::string line;

        while (std::getline(input_file, line))
        {
            std::vector<double> block;
            std::istringstream iss(line);

            std::string delimiter = " ";
            size_t pos = 0;
            std::string token;

            while ((pos = line.find(delimiter)) != std::string::npos) {
                token = line.substr(0, pos);
                block.push_back(stod(token));
                line.erase(0, pos + delimiter.length());
            }

            codebook.push_back(block);
        }

        return codebook;
    }

    double getError(std::vector<std::vector<double>> codebook){
        std::vector<double> new_audio;
        int blocksize = codebook[0].size();
        std::vector<double> block;

        double snr = 0;
        for(size_t i=0; i < this->mono.size(); i++){
            block.push_back(this->mono[i]);
            if ((i+1) % blocksize == 0){
                std::vector<double> closest_block = closestBlock(codebook,block);
                new_audio.insert(std::end(new_audio), std::begin(closest_block), std::end(closest_block));
                block.clear();
            }
        }

        std::vector<std::vector<short>> audio;
        audio.push_back(this->channels[0]);
        std::vector<short> shortVec(new_audio.begin(), new_audio.end());
        audio.push_back(shortVec);

        long signal = WAVCmp::calculateEnergy(audio[0]);
        long noise = WAVCmp::calculateNoise(audio[0], audio[1])[1];
        double long SNR = WAVCmp::SNR(signal, noise);

        std::cout << "Energy: " << signal << "\n";
        std::cout << "Noise: " << noise << "\n";

        return SNR;
    }

    std::vector<double> closestBlock(std::vector<std::vector<double>> codebook,std::vector<double> block){
        std::vector<double> closest_block;

        double min_dist = -1;
        double new_dist;
        double sum;
        for(size_t i=0; i<codebook.size(); i++){
            std::vector<double> aux_block;
            sum = 0.0;
            for(size_t j=0; j<codebook[i].size(); j++){
                sum += pow(codebook[i][j] - block[j], 2.0);
                aux_block.push_back(codebook[i][j]);
            }

            new_dist = sqrt(sum);

            if(new_dist < min_dist || min_dist == -1){
                min_dist = new_dist;
                closest_block = aux_block;
            }
        }
        return closest_block;
    }

    const char kPathSeparator =
        #if defined _WIN32 || defined __CYGWIN__
                    '\\';
        #else
            '/';
        #endif
};


#endif //SHAZEEM_WAVFIND_H

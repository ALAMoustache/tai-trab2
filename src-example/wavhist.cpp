#include <iostream>
#include <vector>
#include <sndfile.hh>
#include "wavhist.h"
#include <boost/algorithm/string.hpp>
//#include "gnuplot-iostream.h"

using namespace std;

constexpr size_t FRAMES_BUFFER_SIZE = 65536; // Buffer for reading frames

const char kPathSeparator =
    #if defined _WIN32 || defined __CYGWIN__
            '\\';
    #else
    '/';
    #endif

int main(int argc, char *argv[]) {

	if(argc < 3) {
		cerr << "Usage: wavhist <input file> <channel>" << endl;
		return 1;
	}

	SndfileHandle sndFile { argv[argc-2] };
	if(sndFile.error()) {
		cerr << "Error: invalid input file" << endl;
		return 1;
    }

	if((sndFile.format() & SF_FORMAT_TYPEMASK) != SF_FORMAT_WAV) {
		cerr << "Error: file is not in WAV format" << endl;
		return 1;
	}

	if((sndFile.format() & SF_FORMAT_SUBMASK) != SF_FORMAT_PCM_16) {
		cerr << "Error: file is not in PCM_16 format" << endl;
		return 1;
	}

	int channel { stoi(argv[argc-1]) };
	if(channel >= sndFile.channels()) {
		cerr << "Error: invalid channel requested" << endl;
		return 1;
	}

    WAVHist hist { sndFile };
	size_t nFrames;
	vector<short> samples(FRAMES_BUFFER_SIZE * sndFile.channels());
    while((nFrames = sndFile.readf(samples.data(), FRAMES_BUFFER_SIZE))) {
        samples.resize(nFrames * sndFile.channels());
        hist.update(samples);
    }

    string filename { argv[argc-2] };
    string output_folder = string("results") + kPathSeparator + string("histograms");
    vector<string> strs;
    boost::split(strs, filename, boost::is_any_of("\\/"));
    const auto histogram_output_path = string("..").append(
            kPathSeparator + output_folder + kPathSeparator + strs.back());
//    cout << "output: " << histogram_output_path << endl;

	hist.dump(channel, histogram_output_path + ".dat");

//	ifstream file;
//    file.open("hist.dat");
//    Gnuplot gp;
////    gp << "set timefmt \"%d/%m/%y,%H:%M\"\n";
////    gp << "set xdata time\n";
////    gp << "plot '-' using 1:2 with lines\n";
//    gp.send(file);
//    file.close()
	return 0;
}


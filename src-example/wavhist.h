#ifndef WAVHIST_H
#define WAVHIST_H

#include <iostream>
#include <vector>
#include <map>
#include <sndfile.hh>
#include <fstream>

class WAVHist {
  private:
	std::vector<std::map<short, size_t>> counts;
	std::map<short, size_t> avg_counts;

  public:
	WAVHist(const SndfileHandle& sfh) {
		counts.resize(sfh.channels());
	}

	WAVHist() { }

	void update(const std::vector<short>& samples) {
		size_t n { };
		auto n_channels = counts.size(); // número de canais
        for (unsigned int i = 0; i < samples.size(); i++){
            auto s = samples[i];
            counts[n++ % n_channels][s]++;
//            std::cout << "mod: " << i % n_channels << '\n';
            if(i % n_channels == 0)
                avg_counts[average(samples, i, n_channels)]++;
        }
	}

    int average(const std::vector<short>& samples, int index, int size){
        auto counter = 0;
//        std::cout << "index + size: " << index << " - " << size << " | " << index+size << '\n';
        for (int i = 0; i < index+size; i++){
            counter += samples[i];
        }
        return int{ counter / size };
	}

	void dump(const size_t channel, const std::string& filename) const {
        std::ofstream generatedFile;
        generatedFile.open(filename);
        if (channel == (size_t) -1) { // Se for mono (-1)
            for(auto channel : counts)
                for(auto [value, counter] : channel)
                    generatedFile << value << '\t' << counter << '\n';
        }else{
            for(auto [value, counter] : counts[channel])
                generatedFile << value << '\t' << counter << '\n';
        }

        generatedFile.close();
	}

    std::vector<short> getMonoSamples(std::vector<std::vector<short>> stereo_channels_samples){
        size_t min_size = stereo_channels_samples[0].size();
        std::vector<short> mono;
        if (min_size > stereo_channels_samples[1].size()){
            min_size = stereo_channels_samples[1].size();
        }

        for(size_t i=0; i<min_size; i++){
            double avg = (stereo_channels_samples[0][i] + stereo_channels_samples[1][i]) / 2;
            mono.push_back(avg);
        }

        return mono;
    }
};

#endif


//
// Created by André Moreira on 07/11/2019.
//

#include <iostream>
#include <vector>
#include <sndfile.hh>
#include "wavquant.h"
#include <boost/algorithm/string.hpp>

using namespace std;

constexpr size_t FRAMES_BUFFER_SIZE = 65536; // Buffer for reading frames

const char kPathSeparator =
    #if defined _WIN32 || defined __CYGWIN__
            '\\';
    #else
    '/';
    #endif

int main(int argc, char *argv[]) {

    if(argc < 4) {
        cerr << "Usage: wavquant <shift step> <input file> <output filename>" << endl;
        return 1;
    }

    int quant_shift { stoi(argv[argc-3]) };
    if(quant_shift < 1 || quant_shift > 15) {
        cerr << "Error: invalid quantization step requested. Quantization step should be higher than 0 and lower than 16" << endl;
        return 1;
    }

    SndfileHandle sndFile { argv[argc-2] };
    if(sndFile.error()) {
        cerr << "Error: invalid input file" << endl;
        return 1;
    }

    if((sndFile.format() & SF_FORMAT_TYPEMASK) != SF_FORMAT_WAV) {
        cerr << "Error: file is not in WAV format" << endl;
        return 1;
    }

    if((sndFile.format() & SF_FORMAT_SUBMASK) != SF_FORMAT_PCM_16) {
        cerr << "Error: file is not in PCM_16 format" << endl;
        return 1;
    }

    string outputFilename { argv[argc-1] };
    string output_folder = string("results") + kPathSeparator + string("quantization");
    vector<string> strs;
    boost::split(strs, outputFilename, boost::is_any_of("\\/"));
    const auto quantFilename = strs.back().append("-quant" + quant_shift).append(".wav");
    const auto filename_output_path = string("..").append(
            kPathSeparator + output_folder + kPathSeparator + quantFilename);
//    cout << "output: " << histogram_output_path << endl;
    SndfileHandle outputFile {filename_output_path, SFM_WRITE, sndFile.format(), sndFile.channels(), sndFile.samplerate()};

    WAVQuant quantizer { quant_shift };
    size_t nFrames;
    vector<short> samples(FRAMES_BUFFER_SIZE * sndFile.channels());
    while((nFrames = sndFile.readf(samples.data(), FRAMES_BUFFER_SIZE))) {
        samples.resize(nFrames * sndFile.channels());

        quantizer.quantization(samples);
        outputFile.writef(samples.data(), nFrames);
    }

    return 0;
}



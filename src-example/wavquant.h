//
// Created by André Moreira on 07/11/2019.
//

#ifndef SHAZEEM_WAVQUANT_H
#define SHAZEEM_WAVQUANT_H


class WAVQuant {
    private:
        int quantizationShift;

    public:
        WAVQuant(int qShift) {
            quantizationShift = qShift;
        }

        void quantization(std::vector<short>& samples){
            for (auto &s: samples){
                s = (s >> quantizationShift) << quantizationShift;
            }
        }


};


#endif //SHAZEEM_WAVQUANT_H
